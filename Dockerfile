FROM rust:1.76-bookworm AS builder

RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y git libffi-dev libssl-dev musl-tools pkg-config
RUN rustup target add x86_64-unknown-linux-musl
RUN rustup component add rustfmt
WORKDIR /project
RUN git clone --depth 1 https://github.com/talaia-labs/rust-teos.git . && git reset --hard a4accedfeecf46edb9c22738ac9787b80dc8af02
RUN RUSTFLAGS='-C target-feature=+crt-static' cargo build --manifest-path=teos/Cargo.toml --locked --release --target x86_64-unknown-linux-musl

FROM alpine:3

WORKDIR /app
COPY --from=builder /project/target/x86_64-unknown-linux-musl/release/teosd ./
COPY --from=builder /project/target/x86_64-unknown-linux-musl/release/teos-cli ./

EXPOSE 9814/tcp

RUN mkdir /root/.teos

ENTRYPOINT [ "/app/teosd" ]
